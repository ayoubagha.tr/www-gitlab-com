require 'date'
require 'hashie'
require 'yaml'

# Generates the Markdown used by the `/releases/` page based on monthly
# release blog posts in this repository
class ReleaseList
  # Don't generate for versions prior to 8.0
  # CUTOFF = Date.new(2015, 9, 22)
  # Don't generate for versions prior to 9.2
  # 9.2 is the first relase post with YAML features
  CUTOFF = Date.new(2017, 5, 22)

  def count(number = StringIO.new)
    # We started release posts with YAML features in 9.2 on May 22nd, 2017.
    # There were 49 releases prior to that on the 22nd until 5.0 on March 22nd, 2013. See - https://about.gitlab.com/community/mvp/
    # There were 5 releases prior to that on or around the 22nd dating back to 3.0.0 release on October 22nd, 2012. See - https://gitlab.com/gitlab-com/www-gitlab-com/issues/5396#note_224693712
    number.puts(release_posts.count + 49 + 5).to_s
    number.string if number.respond_to?(:string)
  end

  def generate(output = StringIO.new)
    release_posts.each do |post|
      output.puts "## [GitLab #{post.version}](#{post.relative_url})"
      output.puts post.date.to_time.strftime('%A, %B %e, %Y').to_s
      output.puts '{: .date}'
      output.puts

      has_stages = false
      ultimate = []
      premium = []
      starter = []
      core = []
      tiers_array = [ultimate, premium, starter, core]
      tiers_names = ["ultimate", "premium", "starter", "core"]
      tier_index = 0

      for tier_item in tiers_array

        # OUTPUT THE TIER HEADER
        tier_name = tiers_names[tier_index]
        output.puts "### #{tier_name.humanize}"

        # INITIALIZE STAGE CONTAINERS
        manage = []
        plan = []
        create = []
        verify = []
        package = []
        secure = []
        release = []
        configure = []
        monitor = []
        defend = []

        # FOR EACH POST, RUN THE HIGHLIGHT FUNCTION
        post.highlights.each do |highlight|

          # FOR EACH HIGHLIGHT, CHECK TO SEE WHAT STAGES ARE IN EACH TIER
          if highlight.stage == "manage" && highlight.tier[0] == tier_name
            manage.push(highlight)
            has_stages = true
          elsif highlight.stage == "plan" && highlight.tier[0] == tier_name
            plan.push(highlight)
            has_stages = true
          elsif highlight.stage == "create" && highlight.tier[0] == tier_name
            create.push(highlight)
            has_stages = true
          elsif highlight.stage == "verify" && highlight.tier[0] == tier_name
            verify.push(highlight)
            has_stages = true
          elsif highlight.stage == "package" && highlight.tier[0] == tier_name
            package.push(highlight)
            has_stages = true
          elsif highlight.stage == "secure" && highlight.tier[0] == tier_name
            secure.push(highlight)
            has_stages = true
          elsif highlight.stage == "release" && highlight.tier[0] == tier_name
            release.push(highlight)
            has_stages = true
          elsif highlight.stage == "configure" && highlight.tier[0] == tier_name
            configure.push(highlight)
            has_stages = true
          elsif highlight.stage == "monitor" && highlight.tier[0] == tier_name
            monitor.push(highlight)
            has_stages = true
          elsif highlight.stage == "defend" && highlight.tier[0] == tier_name
            defend.push(highlight)
            has_stages = true
          end
        end
        # post.highlights.each

        # PUT THE STAGE CONTAINER VARIABLES INTO AN ARRAY FOR ITERATION
        stages_array = [manage, plan, create, verify, package, secure, release, configure, monitor, defend]

        # GIVE THE ARRAY INDEXES SOME NAMES
        stage_names = ["manage", "plan", "create", "verify", "package", "secure", "release", "configure", "monitor", "defend"]

        # DEFINE THE TEMPLATE FOR OUTPUTTING THE LIST OF ITEMS IN EACH STAGE
        def stage_template(output, tier_item, manage, plan, create, verify, package, secure, release, configure, monitor, defend, stages_array, stage_names)
          for stage in stages_array
            stage_index = stages_array.find_index(stage)
            stage_name = stage_names[stage_index].humanize
            if stage.length.positive?
              output.puts "<div class='stage-wrapper'>"
              output.puts "<h5>#{stage_name}</h5><ul>"
              stage.each do |highlight|
                output.puts "<li>#{highlight} "
                categories_array = Array.wrap(highlight.categories)
                for category in categories_array
                  output.puts "<span class='category category-#{category.delete(" ")}'>#{category.humanize}</span> "
                end
                output.puts "</li>"
              end
              output.puts "</ul></div>"
              output.puts
            end
          end
        end

        # RUN THE FUNCTION THAT OUTPUTS THE LIST OF ITEMS IN EACH STAGE
        stage_template(output, tier_item, manage, plan, create, verify, package, secure, release, configure, monitor, defend, stages_array, stage_names)

        # IF NO STAGES, DO THIS INSTEAD
        if has_stages != true
          post.highlights.each do |highlight|
            if highlight.tier[0] == "ultimate" && highlight.tier[0] == tier_name
              ultimate.push(highlight)
            elsif highlight.tier[0] == "premium" && highlight.tier[0] == tier_name
              premium.push(highlight)
            elsif highlight.tier[0] == "starter" && highlight.tier[0] == tier_name
              starter.push(highlight)
            elsif highlight.tier[0] == "core" && highlight.tier[0] == tier_name
              core.push(highlight)
            end
          end
          # post.highlights.each

          #TODO: Put these in an array and iterate over them instead of manually specifying.

          if tier_name == "ultimate"
            ultimate.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

          if tier_name == "premium"
            premium.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

          if tier_name == "starter"
            starter.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

          if tier_name == "core"
            core.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

        end
        # if !has_stages

        tier_index += 1

      end
      # tier_item in tiers_array

    end
    # release_posts.each

    # Return the final string if `output` supports it
    output.string if output.respond_to?(:string)
  end

  # Returns an Array of monthly release posts in descending order
  def release_posts
    root = File.expand_path('../source/releases', __dir__)

    # find's `-regex` option is too dumb to do what we want, so use grep too
    find = %(find "#{root}" -type f -iname "*-released.html.md")
    grep = %(grep #{grep_flags} '\\d{4}-\\d{2}-22-gitlab-\\d{1,2}-\\d{1,2}-released')
    sort = %q(sort -n)

    `#{find} | #{grep} | #{sort}`
      .lines
      .map    { |path| ReleasePost.new(path) }
      .reject { |post| post.date < CUTOFF }
      .reverse
  end

  private

  def grep_flags
    # GNU supports PCRE via `-P`; for others (i.e., BSD), we want `-E`
    if `grep -V`.include?('GNU grep')
      '-P'
    else
      '-E'
    end
  end

  class ReleasePost
    attr_reader :filename, :title, :date, :version, :stage, :categories

    def initialize(filename)
      @filename = filename.strip

      extract_attributes
    end

    def relative_url
      format('/releases/%<year>d/%<month>0.2d/%<day>0.2d/%<title>s', year: date.year, month: date.month, day: date.day, title: title)
    end

    # Returns an Array of "highlights"
    #
    # If a data file exists for the release post, we extract the feature list
    # from its YAML.
    #
    def highlights
      return @highlights if @highlights

      @highlights =
        if data_file?
          highlights_from_data_file
        else
          highlights_from_data_dir
        end

      @highlights
    end

    private

    class Highlight
      attr_reader :title, :link, :tier, :stage, :categories

      def initialize(title, link = nil, tier = nil, stage = nil, categories = nil)
        @title = title
        @link  = link
        @tier = tier
        @stage = stage
        @categories = categories
      end

      def link?
        link.to_s.present?
      end

      def to_s
        if link?
          "<a href='#{link}'>#{title}</a>"
        else
          title
        end
      end
    end

    def extract_attributes
      match = filename.match(
        /
          (?<date>\d{4}-\d{2}-\d{2})
          -
          (?<title>
            gitlab-
            (?<major>\d{1,2})-(?<minor>\d{1,2})
            -released
          )
        /xi
      )

      @title   = match['title']
      @date    = Date.parse(match['date'])
      @version = "#{match['major']}.#{match['minor']}"
    end

    def data_file?
      File.exist?(data_file_path)
    end

    def data_file_path
      filename = File.basename(@filename, '.html.md').tr('-', '_')
      File.expand_path("../data/release_posts/#{filename}.yml", __dir__)
    end

    def highlights_from_data_dir
      features = Hashie::Mash.new

      release_path = File.join(File.expand_path('../data/release_posts', __dir__), @version.tr('.','_'))
      Dir.glob("#{release_path}/*.yml").each do |file_path|
        item = YAML.safe_load(File.read(file_path))

        features.deep_merge!(item) { |key, this_val, other_val| this_val + other_val }
      end

      features
        .fetch('features', {})
        .values
        .flatten
        .collect do |f|
          link = f['documentation_link'] || f['performance_url']
          Highlight.new(f['name'], link, f['available_in'], f['stage'], f['categories'])
        end
    end

    def highlights_from_data_file
      features = YAML.safe_load(File.read(data_file_path)).fetch('features', {})

      features
        .values
        .flatten
        .collect do |f|
          link = f['documentation_link'] || f['performance_url']
          Highlight.new(f['name'], link, f['available_in'], f['stage'], f['categories'])
        end
    end

  end
end
